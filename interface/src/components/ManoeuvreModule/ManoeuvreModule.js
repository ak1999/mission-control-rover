import React, { Component } from 'react';

import socketIOClient from "socket.io-client";
import ReactSpeedometer from "react-d3-speedometer"

import config from '../../config.json';
// import * as socket from './socket.io.js';

import './ManoeuvreModule.css';

const axios = require('axios').default;
var socket = socketIOClient('http://127.0.0.1:5000');


class ManoeuvreModule extends Component {

  constructor(props) {
    super(props);
    this.intervalId = null;
    console.log(config)
    this.state = {
        val: 0
    };
    // axios.get('http://127.0.0.1:5000/')
    //   .then(response => {console.log(response);})
    //   .catch(error => {console.log(error);})
    //   .finally(() => {});

    socket.on('connect',function(){
     
    });
  
  }

  componentDidMount() {
    // TEST
    // this.intervalId = setInterval(() => {
    //   if(!document.getElementById('gps-x')) {
    //     clearInterval(this.intervalId);
    //     return;
    //   }
      
    //   document.getElementById('gps-x').innerHTML = this.getRandomValue();
    //   document.getElementById('gps-y').innerHTML = this.getRandomValue();
    // }, 700)
    // console.log(socket);
    // console.log("hii i am there");
    // socket.on('connect',function(){
    //  var flag = true;
    //  console.log(flag);
    // });
    socket.emit('set-manoeuvre',{ooo:true});
    let _this = this;
   socket.on('manoeuvre response',function(vr){
       console.log(vr.data);
       let scaledVal = vr.data * 250;
       if(scaledVal < 0) scaledVal = 0;
       else if(scaledVal>1000) scaledVal = 1000;

      _this.setState({val: scaledVal})
    //   this.forceUpdate();

   });
   
      
  }
  handleClick(event) {
    console.log("hii set off is launched");
    socket.emit('toggle-manoeuvre',{ooo:true});
  }

  getRandomValue() {
    return (Math.random()*1e4).toFixed(5);
  }

  render() {
    let content;
    if(this.props.module)
      content = <div>{this.props.module.label}</div>
    else
      content = '<div>EMPTY</div>';
      
    return (
      <div>
        <table id="gps-view">
          <tbody>
           <tr>
               <td>
                   <div>
                   <ReactSpeedometer
                   width={200}
                   height={200}
    customSegmentStops={[0, 250, 500, 750, 1000]}
    segmentColors={["firebrick", "tomato", "gold", "limegreen"]}
    value={this.state.val}
  />
                   </div>
             
               </td>
           </tr>
            <tr>
            <td>
                <button id="unique-id" onClick={this.handleClick}>Button</button></td>
            </tr>
           
          </tbody>
        </table>
      </div>
    );
  }
}

export default ManoeuvreModule;