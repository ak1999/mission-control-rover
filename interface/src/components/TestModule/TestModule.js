import React, { Component } from 'react';

import './TestModule.css';

// const axios = require('axios').default;

class TestModule extends Component {

  constructor(props) {
    super(props);
    // console.log(this.props.meta);

    // setInterval(() => {
    //   axios.get('http://127.0.0.1:5000/')
    //   .then(response => {console.log(response);})
    //   .catch(error => {console.log(error);})
    //   .finally(() => {});
    // }, 100);
    
  }

  render() {
    let content;
    if(this.props.module)
      content = <div>{this.props.module.label}</div>
    else
      content = '<div>EMPTY</div>';
      
    return (
      <div>
        {content}
      </div>
    );
  }
}

export default TestModule;