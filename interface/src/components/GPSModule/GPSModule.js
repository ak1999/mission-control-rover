import React, { Component } from 'react';

import socketIOClient from "socket.io-client";

import config from '../../config.json';
// import * as socket from './socket.io.js';

import './GPSModule.css';

const axios = require('axios').default;
var socket = socketIOClient('http://127.0.0.1:5000');


class GPSModule extends Component {

  constructor(props) {
    super(props);
    this.intervalId = null;
    console.log(config)
    // axios.get('http://127.0.0.1:5000/')
    //   .then(response => {console.log(response);})
    //   .catch(error => {console.log(error);})
    //   .finally(() => {});

    socket.on('connect',function(){
     
    });
  
  }

  componentDidMount() {
    // TEST
    // this.intervalId = setInterval(() => {
    //   if(!document.getElementById('gps-x')) {
    //     clearInterval(this.intervalId);
    //     return;
    //   }
      
    //   document.getElementById('gps-x').innerHTML = this.getRandomValue();
    //   document.getElementById('gps-y').innerHTML = this.getRandomValue();
    // }, 700)
    // console.log(socket);
    // console.log("hii i am there");
    // socket.on('connect',function(){
    //  var flag = true;
    //  console.log(flag);
    // });
    socket.emit('set-gps',{ooo:true});

   socket.on('my response',function(vr){
     console.log(vr.data);
   });
   
      
  }
  handleClick(event) {
    // console.log("hii set off is launched");
    socket.emit('toggle-gps',{ooo:true});
  }

  getRandomValue() {
    return (Math.random()*1e4).toFixed(5);
  }

  render() {
    let content;
    if(this.props.module)
      content = <div>{this.props.module.label}</div>
    else
      content = '<div>EMPTY</div>';
      
    return (
      <div>
        <table id="gps-view">
          <tbody>
            <tr>
              <td>
                <div className="ui slider checkbox">
                  <input type="checkbox" name="newsletter" />
                  <label>Accept terms and conditions</label>
                </div>
              </td>
            </tr>
            <tr>
              <td>X:</td>
              <td id="gps-x">2.3234234</td>
            </tr>
            <tr>
              <td>Y:</td>
              <td id="gps-y">4.521334324</td>
            </tr>
            <button id="unique-id" onClick={this.handleClick}>Button</button>
          </tbody>
        </table>
      </div>
    );
  }
}

export default GPSModule;