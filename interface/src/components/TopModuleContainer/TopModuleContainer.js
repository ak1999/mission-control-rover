import React, { Component } from 'react';

import './TopModuleContainer.css';

class TopModuleContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      moduleList: [],
      selectedModule: null,
      lastAddedModule: null,
    };
  }

  render() {

    let component;
    let moduleChanged = false;
    
    if(this.props.module != null) {
      moduleChanged = this.state.lastAddedModule != this.props.module;
    }

    if(this.props.module != null && 
       !this.state.moduleList.includes(this.props.module) && 
       moduleChanged
    ) {
      this.state.moduleList.push(this.props.module);
      this.state.lastAddedModule = this.props.module;
    }

    if(this.state.selectedModule == null && this.state.moduleList.length > 0) {
      this.state.selectedModule = this.state.moduleList[0];
    }
    
    if(this.state.selectedModule == null) {
      component = <div>EMPTY</div>;
    }
    else {
      let Module = this.state.selectedModule.targetComponent;
      component = <Module module={this.state.selectedModule} />;
    }
    
    return (
      <div id="root-top-module">
        <div className="tab-container">
          { this.renderTabs() }
        </div>
        <br/>
        <div>
          {component}
        </div>
      </div>
    );
  }

  removeModule(index) {
    let updatedModuleList = this.state.moduleList;
    
    let updatedSelectedModule = this.state.selectedModule == this.state.moduleList[index] ? null : this.state.selectedModule;
    updatedModuleList.splice(index, 1);

    this.setState({
      moduleList: updatedModuleList,
      selectedModule: updatedSelectedModule,
    });
  }

  selectModule(module) {
    this.setState({selectedModule: module});
  }

  renderTabs() {
    return this.state.moduleList.map((module, index) => {
      return (
        <div 
          className="tab" 
          key={index}
        >
          <div
             className="tab-label" 
             onClick={() => this.selectModule(module)}
          >
            {module.label} 
          </div>
          <div 
            className="tab-close-btn"
            onClick={ ()=> { this.removeModule(index);}}
          >
            
          </div>
        </div>
      );
    })
  }
}

export default TopModuleContainer;
