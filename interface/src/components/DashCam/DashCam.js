import React, { Component } from 'react';

import './DashCam.css';

// const axios = require('axios').default;

class DashCam extends Component {

  constructor(props) {
    super(props);
    // console.log(this.props.meta);

    // setInterval(() => {
    //   axios.get('http://127.0.0.1:5000/')
    //   .then(response => {console.log(response);})
    //   .catch(error => {console.log(error);})
    //   .finally(() => {});
    // }, 100);
    
  }

  render() {
    let content;
    if(this.props.module)
      content = <div>{this.props.module.label}</div>
    else
      content = '<div>EMPTY</div>';
    
    var video = document.querySelector("#videoElement");  
    if (navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia({ video: true })
        .then(function (stream) {
          video.srcObject = stream;
        })
        .catch(function (err0r) {
          console.log("Something went wrong!");
        });
    }  
    
    return (
      <div>
        {content}
        <div id="container">
          <video autoPlay={true} id="videoElement">
          
          </video>
        </div>
      </div>
    );
  }
}

export default DashCam;