import React, { Component } from 'react';

import './BottomModuleContainer.css';

class BottomModuleContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      moduleList: [],
      selectedModule: null,
      lastAddedModule: null,
    };
  }

  render() {

    let component;
    let moduleChanged = false;
    
    if(this.props.module != null) {
      moduleChanged = this.state.lastAddedModule != this.props.module;
    }

    if(this.props.module != null && 
       !this.state.moduleList.includes(this.props.module) && 
       moduleChanged
    ) {
      this.state.moduleList.push(this.props.module);
      this.state.lastAddedModule = this.props.module;
    }

    if(this.state.selectedModule == null && this.state.moduleList.length > 0) {
      this.state.selectedModule = this.state.moduleList[0];
    }
    
    if(this.state.selectedModule == null) {
      component = <div>EMPTY</div>;
    }
    else {
      let Module = this.state.selectedModule.targetComponent;
      component = <Module module={this.state.selectedModule} />;
    }
    
    return (
      <div id="root-bottom-module">
        {this.renderModules()}
      </div>
    );
  }

  removeModule(index) {
    let updatedModuleList = this.state.moduleList;
    
    let updatedSelectedModule = this.state.selectedModule == this.state.moduleList[index] ? null : this.state.selectedModule;
    updatedModuleList.splice(index, 1);

    this.setState({
      moduleList: updatedModuleList,
      selectedModule: updatedSelectedModule,
    });
  }

  selectModule(module) {
    this.setState({selectedModule: module});
  }

  renderModules() {
    return this.state.moduleList.map((module, index) => {
      let Module = module.targetComponent;
      return (
        <div 
          className="module"
          key={index}
        >
          <div className="btm-module-header">
            <div className="btm-module-title">{module.label}</div>
            <div 
              className="btm-module-close-btn"
              onClick={() => this.removeModule(index)}
            >
            </div>
          </div>
          
          <div className="module-content">
            <Module module={module}/>
          </div>
        </div>
      );
    });
  }
}

export default BottomModuleContainer;
