import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Screens
import HomeScreen from './screens/HomeScreen/HomeScreen';

class App extends Component {
  render() {
    return (
      <Router>
        <link rel="stylesheet" type="text/css" href="./thirdparty/semantic-ui.css"></link>
        <link href="https://fonts.googleapis.com/css?family=Karla&display=swap" rel="stylesheet"></link>
        <div className="App">
          <Switch>
            <Route exact path="/" component={HomeScreen} />
          </Switch>
        </div>
      <script src="./thirdparty/socket.io.js" />
      <script src="./thirdparty/semantic-ui.min.js" />
      </Router>
    );
  }
  
}

export default App;
