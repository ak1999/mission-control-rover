import React, { Component } from 'react';
import $ from 'jquery';
import hotkeys from 'hotkeys-js';

import TopModuleContainer from '../../components/TopModuleContainer/TopModuleContainer';
import BottomModuleContainer from '../../components/BottomModuleContainer/BottomModuleContainer';

import './HomeScreen.css';
import DashCam from '../../components/DashCam/DashCam';
import GPSModule from '../../components/GPSModule/GPSModule';
import IMUModule from '../../components/IMUModule/IMUModule';
import ManoeuvreModule from '../../components/ManoeuvreModule/ManoeuvreModule'

class Module {
  constructor(label, targetComponent, meta) {
    this.label = label;
    this.targetComponent = targetComponent;
    this.meta = meta;
  }
}

const moduleList = [
  new Module('DashCam', DashCam, {}),
  new Module('GPS', GPSModule, {}),
  new Module('IMU', IMUModule, {}),
  new Module('Manoeuvre',ManoeuvreModule,{})
];

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      addTopModule: null,
      addBottomModule: null,

      sidemenu_show: true,
    }

    hotkeys('ctrl+b', (event, handler) => {
      this.toggleSidemenu();
    });

  }

  handleOnClick(module) {
    this.setState({addTopModule: module});
  }

  toggleSidemenu() {
    this.setState({sidemenu_show: !this.state.sidemenu_show});
  }

  hideSidemenu() {
    $('.inventory-tab').css({display: 'none'});
    $("#root-module-inventory").css({width: '0px'});
  }

  showSidemenu() {
    let el = document.getElementById('root-module-inventory');
    el.style.width = '180px';
    $('.inventory-tab').css({display: 'table-row'});

  }

  componentDidMount() {
    if(this.state.sidemenu_show)
      this.showSidemenu();
    else
      this.hideSidemenu();
  }

  componentDidUpdate() {
    if(this.state.sidemenu_show)
      this.showSidemenu();
    else
      this.hideSidemenu();
  }

  render() {
    return (
      <div id="root-home">
        <div id="root-module-inventory">
          <div 
            id="menu-toggle"
            onClick={() => this.toggleSidemenu()}
          >></div>
          { 
            <table border="1">
              <tbody>
                {this.renderModuleButtons()}
              </tbody>
            </table>
          }
        </div>
        <TopModuleContainer 
          module={this.state.addTopModule}
        />
        <BottomModuleContainer
          module={this.state.addBottomModule}
        />
      </div>
    );
  }

  triggerTopContainer(module) {this.state.addBottomModule = null; this.setState({addTopModule: module}); }
  triggerBottomContainer(module) { this.state.addTopModule = null; this.setState({addBottomModule: module}); }

  renderModuleButtons() {
    return moduleList.map((module, index) => {
      return (
        <tr key={index} className="inventory-tab">
          <td className="tab-label">{module.label}</td>
          <td className="tab-button" onClick={() => this.triggerTopContainer(module)}>UP</td>
          <td className="tab-button" onClick={() => this.triggerBottomContainer(module)}>BTM</td>
        </tr>
      );
    });
  }
}

export default HomeScreen;