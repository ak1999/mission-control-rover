from flask import Flask
from flask_cors import CORS
from flask_socketio import SocketIO
import threading
import sys, os
import time
import keyboard

app = Flask(__name__)
app.config['SECRET_KEY'] = 'key'
cors = CORS(app)
socketio = SocketIO(app, cors_allowed_origins="*")
stop_threads = False


class Module:

  def __init__(self,meta,fun,*args):
    self.meta = meta
    self.my_args = args
    self.my_function = fun
    self.mythread = threading.Thread(target=self.toRun)
    self.state = False
    self.forward_speed = 0
    # 0 is backward and 1 is forward
    self.direction = 1

  
  def toRun(self):
    while(True):
      self.my_function(*self.my_args)
      if(self.state):
        break
  
  def State(self,val):
    self.state = val
    if(self.state == False):
      self.mythread = threading.Thread(target=self.toRun)
      self.startThread()
  
  def startThread(self):
    self.mythread.start()



  
  


def print_square(num):
  print("Square: {}".format(num * num))
  socketio.emit('my response', {'data': 'Connected'})
  time.sleep(0.05)

def print_cube(num):
  while True:  # making a loop
    # print("intertation")
    try:  # used try so that if user pressed other than the given key error will not be shown
        if keyboard.is_pressed('w'):  # if key 'q' is pressed
            if manoeuvreModule.forward_speed<5:
              manoeuvreModule.forward_speed = manoeuvreModule.forward_speed + 1
            val = manoeuvreModule.forward_speed
            socketio.emit('manoeuvre response', {'data': val}) # finishing the loop
            time.sleep(0.34)
            break
        elif keyboard.is_pressed('s'):
            if manoeuvreModule.forward_speed>-5:
              manoeuvreModule.forward_speed = manoeuvreModule.forward_speed -1
            val = manoeuvreModule.forward_speed
            socketio.emit('manoeuvre response', {'data': val})
            time.sleep(0.34)
            break
    except:
        break  # if user pressed a key other than the given key the loop will break
  time.sleep(0.05)

gpsModule = Module("this is meta",print_square,10)
manoeuvreModule = Module("this is meta",print_cube,10)
def blockPrint():
    sys.stdout = open(os.devnull, 'w')


@socketio.on('my event')
def handle_my_custom_event(json):
  print('received json: ' + str(json))

@socketio.on('set-gps')
def gps_handle(json):
  print("I am launched in python server by react server")
  time.sleep(1)
  gpsModule.state = False 
  gpsModule.startThread()
  print("I am started")  

@socketio.on('set-manoeuvre')
def manoeuvre_handle(json):
  print("I am manoeuvre in python server")
  time.sleep(1)
  manoeuvreModule.state = False
  manoeuvreModule.startThread()
  print('manoeuvre module started')

@socketio.on('toggle-gps')
def gps_off(json):
  print("Hii this is gps_off")
  time.sleep(29)
  print('received gps state: ' + str(json))
  val = gpsModule.state
  val = not val
  gpsModule.State(val)

@socketio.on('toggle-manoeuvre')
def manoeuvre_off(json):
  print("Hii this is manoeuvre off")
  print('received gps state: ' + str(json))
  val = manoeuvreModule.state
  val = not val
  manoeuvreModule.State(val)

@app.route('/')
def hellow_world():
  print('hello')
  return 'Hello World'

if __name__ == "__main__":
  app.run(debug=False)

